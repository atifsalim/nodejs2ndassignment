const Company = require("../models/company");

exports.getCreatecompany = (req, res, next) => {
  console.log(req.body, "req");
  const name = req.body.name;
  console.log(name);
  const company = new Company(null, name);
  company.save();
  res.send({
    success: "true",
    message: "Company added successfully",
  });
};

exports.getUpdatecompany = (req, res, next) => {
  const compId = req.params.companyId;
  console.log(req.params);
  Company.findById(compId, (company) => {
    console.log(company, "in update");
    if (!company) {
      return res.send({
        success: "fail",
        message: "Data is not stored in Database",
      });
    } else {
      const conpanyId = req.body.companyId;
      const companyName = req.body.name;

      const updatedCompany = new Company(conpanyId, companyName);
      updatedCompany.save();
    }
    return res.send({
      success: "true",
      message: "Company updated successfully",
    });
  });
};

exports.getcompanies = (req, res, next) => {
  Company.fetchAll((companies) => {
    res.send({
      success: "true",
      companies,
    });
  });
};

exports.postDeletecompany = (req, res, next) => {
  const compId = req.body.companyId;
  Company.deleteById(compId);
  res.send({
    success: "true",
    message: "Company deleted Successfully",
  });
};
