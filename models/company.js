const fs = require("fs");
const path = require("path");

const p = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "componies.json"
);

const getComponiesFromFile = (cb) => {
  fs.readFile(p, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class Company {
  constructor(id, name) {
    this._id = id;
    this.name = name;
  }

  save() {
    getComponiesFromFile((companies) => {
      if (this._id) {
        const existingCompanyIndex = companies.findIndex(
          (comp) => comp._id === this._id
        );
        const updatedCompanies = [...companies];
        updatedCompanies[existingCompanyIndex] = this;
        fs.writeFile(p, JSON.stringify(updatedCompanies), (err) => {
          console.log(err);
        });
      } else {
        this._id = new Date().getTime();
        companies.push(this);
        fs.writeFile(p, JSON.stringify(companies), (err) => {
          console.log(err);
        });
      }
    });
  }

  static deleteById(id) {
    getComponiesFromFile((companies) => {
      const updatedCompanies = companies.filter((comp) => comp._id !== id);
      fs.writeFile(p, JSON.stringify(updatedCompanies), (err) => {
        console.log(err);
      });
    });
  }

  static fetchAll(cb) {
    getComponiesFromFile(cb);
  }

  static findById(id, cb) {
    getComponiesFromFile((companies) => {
      const company = companies.find((c) => c._id == id);
      cb(company);
    });
  }
};
