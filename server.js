const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const PORT = 8080;

app.set("view engine", "pug");
app.set("views", "views");

const adminRoutes = require("./routes/admin");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", adminRoutes);

app.get("/about-us", (req, res, next) => {
  res.render("about-us", {
    pageTitle: "About Us",
    path: "about-us",
  });
});
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
